package com.github.welblade.ita.locadora;

public class NewRelease extends Movie {
    public NewRelease(String title){
        super(title);
    }

    double getAmount(int daysRented) {
        return daysRented * 3;
    }

    int getFrequentRenterPoints(int daysRented) {
		//add bonus for a two day new release rental
		if (daysRented > 1)
			return 2;
		return 1;
	};
}
