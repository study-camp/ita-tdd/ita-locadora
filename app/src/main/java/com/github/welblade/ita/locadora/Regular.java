package com.github.welblade.ita.locadora;

public class Regular extends Movie {
    public Regular(String title){
        super(title);
    }

    double getAmount(int daysRented) {
        if (daysRented > 2)
            return 2 + (daysRented - 2) * 1.5;
        return 2;
    }
}
